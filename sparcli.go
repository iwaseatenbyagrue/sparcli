package sparcli

import (
	"fmt"
	"strings"
)

// https://en.wikipedia.org/wiki/ANSI_escape_code#3-bit_and_4-bit
var fgColours = map[string]int{
	"black":   30,
	"red":     31,
	"green":   32,
	"yellow":  33,
	"blue":    34,
	"magenta": 35,
	"cyan":    36,
	"white":   37,
}

func Bold(s string) string {
	return fmt.Sprintf("\033[1m%s\033[22m", s)
}

func Dim(s string) string {
	return fmt.Sprintf("\033[2m%s\033[22m", s)
}

func Italics(s string) string {
	return fmt.Sprintf("\033[3m%s\033[23m", s)
}

func Overline(s string) string {
	return fmt.Sprintf("\033[53m%s\033[55m", s)
}

func Underline(s string) string {
	return fmt.Sprintf("\033[4m%s\033[24m", s)
}

func Strikethrough(s string) string {
	return fmt.Sprintf("\033[9m%s\033[29m", s)
}

func Frame(s string) string {
	return fmt.Sprintf("\033[51m%s\033[54m", s)
}

func Encircle(s string) string {
	return fmt.Sprintf("\033[52m%s\033[54m", s)
}

func Blink(s string, fast bool) string {
	c := 5

	if fast {
		c = 6
	}

	return fmt.Sprintf("\033[%dm%s\033[25m", c, s)
}

func Colour(s string, colour string, bright bool) string {

	var c int
	var ok bool

	if c, ok = fgColours[strings.ToLower(colour)]; !ok {
		return s
	}

	if bright {
		return fmt.Sprintf("\033[1;%dm%s\033[39m", c, s)
	}

	return fmt.Sprintf("\033[%dm%s\033[39m", c, s)

}

func Colour8Bit(s string, code int) string {
	return fmt.Sprintf("\033[38;5;%dm%s\033[39m", code, s)
}

func RGB(s string, r, g, b int) string {
	return fmt.Sprintf("\033[38;2;%d;%d;%dm%s\033[39m", r, g, b, s)
}

func BackgroundColour(s string, colour string, bright bool) string {

	var c int
	var ok bool

	if c, ok = fgColours[strings.ToLower(colour)]; !ok {
		return s
	}

	if bright {
		c = c + 60
	}
	// Background colours are the foreground colour + 10.
	c = c + 10
	return fmt.Sprintf("\033[%dm%s\033[49m", c, s)
}

func BackgroundColour8Bit(s string, code int) string {
	return fmt.Sprintf("\033[48;5;%dm%s\033[49m", code, s)
}

func BackgroundRGB(s string, r, g, b int) string {
	return fmt.Sprintf("\033[48;2;%d;%d;%dm%s\033[49m", r, g, b, s)
}

func Reset(s string) string {
	return fmt.Sprintf("%s\033[0m", s)
}

func Invert(s string) string {
	return fmt.Sprintf("\033[7m%s", s)
}
