# sparcli

A collection of functions for formatting CLI output.

`sparcli` provides:

* **bold**, <u>underlined</u>, *italics* and more.
* colours! Use named (i.e. 3/4 bit) colours, 8 bit colour codes, or RGB, for foreground or background.
* invert colours, reset colours and formatting, blink .


## Install

```
go get gitlab.com/iwaseatenbyagrue/sparcli
```

## Usage


```
package main

import(
  "fmt"
  "gitlab.com/iwaseatenbyagrue/sparcli"
)


func main() {
    fmt.Println(sparcli.Reset(sparcli.Colour("BRIGHT GREEN!!!!", "green", true))
}
```
